#  Copyright © 2016-2019 RebornOS
#
#  This file is part of RebornOS.
#
#  Reborn OS is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  Reborn OS is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  The following additional terms are in effect as per Section 7 of the license:
#
#  The preservation of all legal notices and author attributions in
#  the material or in the Appropriate Legal Notices displayed
#  by works containing it is required.
#
#  You should have received a copy of the GNU General Public License
#  along with Reborn OS; If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash
#####################################
# Script to make ISO testing easier #
#####################################

QUESTION(){
echo
echo "Please select your preferred course of action:"
echo
options=("Build an ISO" "Update code to latest stuff on Gitlab" "Change Branches" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Build an ISO")
            BUILD;break;;
        "Update code to latest stuff on Gitlab")
            UPDATE;break;;
        "Change Branches")
           BRANCHES;break;;
        "Quit")
            break
            ;;
        *) echo "ERROR!!! ERROR!!!! SOUND THE ALARM!!!" 
            echo "Sadly, option $REPLY is not possible! Please select either option 1, 2, or 3 instead. Thank you!";;
    esac
done
}

BRANCHES(){
touch /tmp/branch.txt
yad --form --separator='\n' \
    --field="Branch:cb" "master!development!">/tmp/branch.txt \
MY_BRANCH=$(sed '1q;d' /tmp/branch.txt)
sudo git checkout $(sed '1q;d' /tmp/branch.txt)
echo
echo
echo "DONE"
rm -f /tmp/branch.txt
echo
echo
}

BUILD(){
echo "ENSURING ALL DEPENDENCIES ARE ALREADY INSTALLED..."
sudo pacman -S arch-install-scripts cpio dosfstools git libisoburn mkinitcpio-nfs-utils make patch squashfs-tools wget lynx archiso yad --noconfirm --needed
echo
if [ -f ./work/pacman.conf ]; then
echo "REMOVING FILES FROM PREVIOUS BUILD..."
rm -rf ./work 
echo "WELL THAT TOOK AWHILE! BUT NOW WE'RE DONE :)"
fi
echo
echo 
echo "###################"
echo "# BUILDING ISO... #"
echo "###################"
echo
echo
./build.sh -v
}

UPDATE(){
if [ -f ./work/pacman.conf ]; then
echo
echo "CLEANING EVERYTHING FRIST FROM PREVIOUS BUILDS..."
rm -rf ./work 
echo
echo "WELL THAT TOOK AWHILE! BUT NOW WE'RE DONE :)"
fi
echo
echo
echo "UPDATING TO THE LATEST AND GREATEST..."
sudo git pull
echo
echo "DONE"
}

export -f QUESTION BUILD UPDATE BRANCHES
QUESTION
